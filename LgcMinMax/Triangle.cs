﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LgcMinMax
{
    public class Triangle
    {
        public double left;
        public double center;
        public double right;
        public string name = "Untitled";

        public EventHandler NumberChanged;

        private void check(double a, double b, double c)
        {
            if (a >= b || c <= b)
                throw new InvalidOperationException("Error: uncorrect triangle number! ");
        }

        public void parse(string text)
        {
            string[] numpair = text.Split(';');
            double a, b, c;

            if (numpair.Length != 3)
            {
                throw new FormatException("Enter 3 number separeted by \";\" ");
            }

            if (!double.TryParse(numpair[0], out a))
                throw new FormatException("Uncorrect first number");
            if (!double.TryParse(numpair[1], out b))
                throw new FormatException("Uncorrect second number");
            if (!double.TryParse(numpair[2], out c))
                throw new FormatException("Uncorrect third number");
            check(a, b, c);//thrown
            left = a;
            center = b;
            right = c;
            if (NumberChanged != null)
                NumberChanged(this, EventArgs.Empty);
        }

        public Triangle(double lhs, double center, double rhs)
        {
            this.left = lhs;
            this.center = center;
            this.right = rhs;
        }
        public Triangle(string name, double lhs, double center, double rhs)
        {
            this.left = lhs;
            this.center = center;
            this.right = rhs;
            this.name = name;
        }

        /*
        Метод обраховує точку перетину прямих в області y=[0;1], x=R для прямих
        {(a1;0) i (a2;1)} та {(b1;0) i (b2;1)}
        Повертає масив двох чисел [x,y] або null (якщо перетину немає).
        */
        private static double[] intersect(double a1, double a2, double b1, double b2)
        {
            double zn = b2 - b1 - a2 + a1;
            if (a1 == a2 || b1 == b2 || zn == 0)
                return null;
            double x = (a1 * b2 - a2 * b1) / zn;
            double y = (x - a1) / (a2 - a1);
            if (y < 1 && y>0)
                return new double[] { x, y };
            else return null;
        }
        /*
         Виконує розрахунок вузлових точок ламаної результуючого нечіткого числа.
         Повертає масив кожен з елементів якого є масив чисел [x,y]. Іншими словами повертає масив точок,
         де кожна точка масив double довжиною 2.
        */
        private static double[][] extrmum(Triangle a, Triangle b, Func<double, double, bool> extremumType)
        {
            List<double[]> result = new List<double[]>();
            result.Add(new double[] { extremumType(a.left , b.left) ? a.left : b.left, 0 });
            double[] Lintersect = intersect(a.left, a.center, b.left, b.center);
            if (Lintersect != null)
                result.Add(Lintersect);
            result.Add(new double[] { extremumType(a.center , b.center) ? a.center : b.center, 1 });
            double[] Rintersect = intersect(a.right, a.center, b.right, b.center);
            if (Rintersect != null)
                result.Add(Rintersect);
            result.Add(new double[] { extremumType(a.right , b.right) ? a.right : b.right, 0 });
            return result.ToArray();
        }

        
  

        public static string getMask(Triangle c, Triangle d)
        {
            return (c.left < d.left ? "1" : "0") + (c.center < d.center ? "1" : "0") + (c.right < d.right ? "1" : "0");
        }

        public static string getModelName(string mask)
        {
            return "SM" + ((mask[0] == '0' ? 4 : 0) + (mask[1] == '0' ? 2 : 0) + (mask[2] == '0' ? 1 : 0) + 1);
        }
        
        public static string[] directModel(double [][] nodes, Triangle c, Triangle d)
        {
            string mask = getMask(c, d);

            int linesCount = 3 + (mask[0] != mask[1] ? 1 : 0) + (mask[1] != mask[2] ? 1 : 0);
            var lines = new String[linesCount];

            lines[0] = string.Format("0,∀(y≤{0})∪(y≥{1})", nodes[0][0], nodes[nodes.Length - 1][0]);

            if (mask[0] == '0' && mask[1] == '0' && mask[2] == '0')
            {
                lines[1] = c.LExpr(nodes[0][0], nodes[1][0]);
                lines[2] = c.RExpr(nodes[1][0], nodes[2][0]);
            }
            else if (mask[0] == '0' && mask[1] == '0' && mask[2] == '1')
            {
                lines[1] = c.LExpr(nodes[0][0], nodes[1][0]);

                lines[2] = c.RExpr(nodes[1][0], nodes[2][0]);
                lines[3] = d.RExpr(nodes[2][0], nodes[3][0]);
            }
            else if (mask[0] == '0' && mask[1] == '1' && mask[2] == '0')
            {
                lines[1] = c.LExpr(nodes[0][0], nodes[1][0]);
                lines[2] = d.LExpr(nodes[1][0], nodes[2][0]);

                lines[3] = d.RExpr(nodes[2][0], nodes[3][0]);
                lines[4] = c.RExpr(nodes[3][0], nodes[4][0]);
            }
            else if (mask[0] == '0' && mask[1] == '1' && mask[2] == '1')
            {
                lines[1] = c.LExpr(nodes[0][0], nodes[1][0]);
                lines[2] = d.LExpr(nodes[1][0], nodes[2][0]);

                lines[3] = d.RExpr(nodes[2][0], nodes[3][0]);
            }
            else if (mask[0] == '1' && mask[1] == '0' && mask[2] == '0')
            {
                lines[1] = d.LExpr(nodes[0][0], nodes[1][0]);
                lines[2] = c.LExpr(nodes[1][0], nodes[2][0]);
                
                lines[3] = c.RExpr(nodes[2][0], nodes[3][0]);
            }
            else if (mask[0] == '1' && mask[1] == '0' && mask[2] == '1')
            {
                lines[1] = d.LExpr(nodes[0][0], nodes[1][0]);
                lines[2] = c.LExpr(nodes[1][0], nodes[2][0]);

                lines[3] = c.RExpr(nodes[2][0], nodes[3][0]);
                lines[4] = d.RExpr(nodes[3][0], nodes[4][0]);
            }
            else if (mask[0] == '1' && mask[1] == '1' && mask[2] == '0')
            {
                lines[1] = d.LExpr(nodes[0][0], nodes[1][0]);

                lines[2] = d.RExpr(nodes[1][0], nodes[2][0]);
                lines[3] = c.RExpr(nodes[2][0], nodes[3][0]);
            }
            else if (mask[0] == '1' && mask[1] == '1' && mask[2] == '1')
            {
                lines[1] = d.LExpr(nodes[0][0], nodes[1][0]);
                
                lines[2] = d.RExpr(nodes[1][0], nodes[2][0]);
            }
            return lines;

        }

        public static string[][] inverseModel(double[][] nodes, Triangle c, Triangle d)
        {
            string mask = getMask(c, d);
            
            var linesLeft = new String[1 + (mask[0] != mask[1] ? 1 : 0)];
            var linesRight = new String[1 + (mask[1] != mask[2] ? 1 : 0)];


            if (mask[0] == '0' && mask[1] == '0' && mask[2] == '0')
            {
                linesLeft[0] = c.LAlphaExpr(nodes[0][1], nodes[1][1]);
                linesRight[0] = c.RAlphaExpr(nodes[1][1], nodes[2][1]);
            }
            else if (mask[0] == '0' && mask[1] == '0' && mask[2] == '1')
            {
                linesLeft[0] = c.LAlphaExpr(nodes[0][1], nodes[1][1]);

                linesRight[0] = c.RAlphaExpr(nodes[1][1], nodes[2][1]);
                linesRight[1] = d.RAlphaExpr(nodes[2][1], nodes[3][1]);
            }
            else if (mask[0] == '0' && mask[1] == '1' && mask[2] == '0')
            {
                linesLeft[0] = c.LAlphaExpr(nodes[0][1], nodes[1][1]);
                linesLeft[1] = d.LAlphaExpr(nodes[1][1], nodes[2][1]);

                linesRight[0] = d.RAlphaExpr(nodes[2][1], nodes[3][1]);
                linesRight[1] = c.RAlphaExpr(nodes[3][1], nodes[4][1]);
            }
            else if (mask[0] == '0' && mask[1] == '1' && mask[2] == '1')
            {
                linesLeft[0] = c.LAlphaExpr(nodes[0][1], nodes[1][1]);
                linesLeft[1] = d.LAlphaExpr(nodes[1][1], nodes[2][1]);

                linesRight[0] = d.RAlphaExpr(nodes[2][1], nodes[3][1]);
            }
            else if (mask[0] == '1' && mask[1] == '0' && mask[2] == '0')
            {
                linesLeft[0] = d.LAlphaExpr(nodes[0][1], nodes[1][1]);
                linesLeft[1] = c.LAlphaExpr(nodes[1][1], nodes[2][1]);

                linesRight[0] = c.RAlphaExpr(nodes[2][1], nodes[3][1]);
            }
            else if (mask[0] == '1' && mask[1] == '0' && mask[2] == '1')
            {
                linesLeft[0] = d.LAlphaExpr(nodes[0][1], nodes[1][1]);
                linesLeft[1] = c.LAlphaExpr(nodes[1][1], nodes[2][1]);

                linesRight[0] = c.RAlphaExpr(nodes[2][1], nodes[3][1]);
                linesRight[1] = d.RAlphaExpr(nodes[3][1], nodes[4][1]);
            }
            else if (mask[0] == '1' && mask[1] == '1' && mask[2] == '0')
            {
                linesLeft[0] = d.LAlphaExpr(nodes[0][1], nodes[1][1]);

                linesRight[0] = d.RAlphaExpr(nodes[1][1], nodes[2][1]);
                linesRight[1] = c.RAlphaExpr(nodes[2][1], nodes[3][1]);
            }
            else if (mask[0] == '1' && mask[1] == '1' && mask[2] == '1')
            {
                linesLeft[0] = d.LAlphaExpr(nodes[0][1], nodes[1][1]);

                linesRight[0] = d.RAlphaExpr(nodes[1][1], nodes[2][1]);
            }
            return new string[][] { linesLeft, linesRight };

        }



 

        public static double[][] max(Triangle a, Triangle b)
        {
            return extrmum(a, b, (x, y) => x > y);
        }
        public override string ToString()
        {
            return string.Format("{0};{1};{2}", Math.Round(left, 3), Math.Round(center, 3), Math.Round(right, 3));
        }

 

        private string numberWithSign(double n)
        {
            if (n >= 0) return string.Format("+{0}", n);
            else return n.ToString();
        }

  

        public string LExpr(double from, double to)
        {
            return string.Format((left != 0 ? "(y{3})" : "y") + "/{2}, ∀({0}≤y≤{1})", from, to, center - left, numberWithSign(-left));
        }

        public string RExpr(double from, double to)
        {
            return string.Format((right != 0 ? "({3}-y)" : "-y") + "/{2}, ∀({0}≤y≤{1})", from, to, right - center, right);
        }

        public string LAlphaExpr(double from, double to)
        {
            return this.left + " + " + (this.center - this.left) + "α,∀α|α∈[" + from + "," + to + "]";
        }

        public string RAlphaExpr(double to, double from)
        {
            return this.right + " - " + (this.right - this.center) + "α,∀α|α∈["+from+","+to+"]";
        }

        public double[] calculateInterval(double alpha)
        {
            double a = this.left + (this.center - this.left) * alpha;
            double b = this.right - (this.right - this.center) * alpha;
            return new double[] { a, b };
        }
        

        public static string intervalExprOfMax(Triangle a, Triangle b, double alpha)
        {
            if (alpha < 0 || alpha > 1)
                return "undefined";
            var aInt = a.calculateInterval(alpha);
            var bInt = b.calculateInterval(alpha);
            var left = Math.Max(aInt[0], bInt[0]);
            var right = Math.Max(aInt[1], bInt[1]);
            return string.Format("[{0},{1}]", left, right);
        }

        public double calculateMu(double x)
        {
            if (x >= this.left && x <= this.center)
            {
                return (x - this.left) / (this.center - this.left);
            }
            else if (x >= this.center && x <= this.right)
            {
                return (this.right - x) / (this.right - this.center);
            }
            return 0;
        }

        public static string muExprOfMax(Triangle a, Triangle b, double x)
        {
            var aMu = a.calculateMu(x);
            var bMu = b.calculateMu(x);
            return Math.Max(aMu, bMu).ToString();
        }

    }
}
