﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace LgcMinMax
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Series ser = new Series();
            ser.Name = "a";
            prepairSeries(ser);

            Series ser2 = new Series();
            ser2.Name = "b";
            prepairSeries(ser2);

            chart.Series["a"] = ser;
            chart.Series["b"] = ser2;

            chart.Series["alpha*"] = new Series("alpha*");
            chart.Series["alpha*"].Name = "alpha*";
            prepairSeries(chart.Series["alpha*"]);

            chart.Series["alpha**"] = new Series("alpha**");
            chart.Series["alpha**"].Name = "alpha**";
            prepairSeries(chart.Series["alpha**"]);

            chart.Series["hOne"] = new Series("hOne");
            chart.Series["hOne"].Name = "hOne";
            prepairSeries(chart.Series["hOne"]);

            setFontSize(14);

        }
        

        private void prepairSeries(Series ser) {
            ser.ChartType = SeriesChartType.Line;
            ser.BorderWidth = 1;
            ser.Color = Color.Black;
            ser.BorderDashStyle = ChartDashStyle.DashDot;
            ser.IsVisibleInLegend = false;
        }

        private void showLabels(double?[] alphas) {

            DataPointCollection coll = chart.Series["labels"].Points;
            coll.Clear();

            var interval = chart.ChartAreas["ChartArea1"].AxisX.Interval;

            DataPoint p = new DataPoint(a.left, 0);
            p.LegendText = "  c1";
            coll.Add(p);
            p = new DataPoint(a.center, 0);
            p.LegendText = "c0";
            coll.Add(p);
            p = new DataPoint(a.right, 0);
            p.LegendText = "c2";
            coll.Add(p);

            p = new DataPoint(b.left, 0);
            p.LegendText = "d1";
            coll.Add(p);
            p = new DataPoint(b.center, 0);
            p.LegendText = "d0";
            coll.Add(p);
            p = new DataPoint(b.right, 0);
            p.LegendText = "d2";
            coll.Add(p);

            if (alphas[0] != null)
            {
                p = new DataPoint(0, (double)alphas[0]);
                coll.Add(p);
                p = new DataPoint(0.25*interval, (double)alphas[0]+0.05);
                p.MarkerImage = "aL";
                coll.Add(p);
            }
            if (alphas[1] != null)
            {
                p = new DataPoint(0, (double)alphas[1]);
                coll.Add(p);
                p = new DataPoint(0.25*interval, (double)alphas[1]+0.05);
                p.MarkerImage = "aR";
                coll.Add(p);
            }

        }

        Triangle a = new Triangle(0, 0, 0), b = new Triangle(0, 0, 0);

        private void drawTriangle(int serIndex, Triangle a)
        {
            DataPoint point;
            chart.Series[serIndex].Points.Clear();
            point = new DataPoint(a.left, 0);
            chart.Series[serIndex].Points.Add(point);
            point = new DataPoint(a.center, 1);
            chart.Series[serIndex].Points.Add(point);
            point = new DataPoint(a.right, 0);
            chart.Series[serIndex].Points.Add(point);

        }

        private void drawV(string name, double h)
        {
            DataPointCollection coll = chart.Series[name].Points;
            coll.Clear();
            coll.Add(new DataPoint(h, 0));
            coll.Add(new DataPoint(h, 1));
        }

        private void drawH(string name, double x, double y)
        {
            DataPointCollection coll = chart.Series[name].Points;
            coll.Clear();
            coll.Add(new DataPoint(x, y));
            coll.Add(new DataPoint(0, y));
        }

        private void drawOne() {
            double min = a.left < b.left ? a.left : b.left;
            double max = a.right > b.right ? a.right : b.right;
            min = min > 0 ? 0 : min;
            DataPointCollection col = chart.Series["hOne"].Points;
            col.Clear();
            col.Add(new DataPoint(min, 1));
            col.Add(new DataPoint(max, 1));
        }

        

        

        private void parse()
        {
            try
            {
                a.parse(textBoxA.Text);
                b.parse(textBoxB.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void paintChart(){

            var axisX = chart.ChartAreas["ChartArea1"].AxisX;

            axisX.Interval = (Math.Max(a.right, b.right) - Math.Min(a.left, b.left)) / 5;
            

            chart.Series["alpha*"].Points.Clear();
            chart.Series["alpha**"].Points.Clear();


            drawTriangle(0, a);
            drawTriangle(1, b);

            chart.Series[0].Name = string.Format("C=({0})", a);
            chart.Series[1].Name = string.Format("D=({0})", b);
            chart.Series[2].Name = "S=C(V)D";

            drawV("a", a.center);
            drawV("b", b.center);

            DataPointCollection respcoll = chart.Series[2].Points;
            respcoll.Clear();

            double?[] alphas = new double?[2];

            foreach (var point in Triangle.max(a, b))
            {
                respcoll.Add(new DataPoint(point[0] + axisX.Interval/20, point[1]));
                if (point[0] != a.left && point[0] != a.right && point[0] != b.left && point[0] != b.right)
                {
                    if (point[0] > a.center && point[0] > b.center)
                    {
                        drawH("alpha**", point[0], point[1]);
                        alphas[1] = point[1];
                    }
                    else if (point[0] < a.center && point[0] < b.center)
                    {
                        drawH("alpha*", point[0], point[1]);
                        alphas[0] = point[1];
                    }
                }
            }

            showLabels(alphas);
            drawOne();
            axisX.RoundAxisValues();
        }


        private void setFontSize(int s)
        {
            chart.Series["labels"].Font = new Font("Microsoft Sans Serif", s);
            chart.ChartAreas[0].Axes[0].LabelAutoFitMaxFontSize = s;
            chart.ChartAreas[0].Axes[0].LabelAutoFitMinFontSize = s;
            chart.ChartAreas[0].Axes[0].TitleFont = new Font("Microsoft Sans Serif", s);
            chart.ChartAreas[0].Axes[1].LabelAutoFitMaxFontSize = s;
            chart.ChartAreas[0].Axes[1].LabelAutoFitMinFontSize = s;
            chart.ChartAreas[0].Axes[1].TitleFont = new Font("Microsoft Sans Serif", s);
            chart.Legends[0].Font = new Font("Microsoft Sans Serif", s);
        }

        private void muCalc_Click(object sender, EventArgs e)
        {
            if (a==null || b == null)
            {
                MessageBox.Show("Enter triangle numbers and press Calculate before this action!");
                return;
            }
            double y0 = 0;
            if (!double.TryParse(y0V.Text, out y0))
            {
                MessageBox.Show("Uncorrect number!");
                return;
            }
            muRes.Text = Triangle.muExprOfMax(a, b, y0);
        }

        private void intCalc_Click(object sender, EventArgs e)
        {
            if (a == null || b == null)
            {
                MessageBox.Show("Enter triangle numbers and press Calculate before this action!");
                return;
            }
            double mu0 = 0;
            if (!double.TryParse(alpha0V.Text, out mu0))
            {
                MessageBox.Show("Uncorrect number!");
                return;
            }
            intRes.Text = Triangle.intervalExprOfMax(a, b, mu0);
        }

        private void button_Click(object sender, EventArgs e)
        {

            parse();
            StringBuilder infoText = new StringBuilder();
            var mask = Triangle.getMask(a, b);
            infoText.Append("Mask {m1,m2,m3} = {" + mask[0] + "," + mask[1] + "," + mask[2] + "}\r\n");
            infoText.AppendFormat("Model: {0}\r\n", Triangle.getModelName(mask));
            var res = Triangle.max(a, b);
            dModel.Lines = Triangle.directModel(res, a, b);
            var inverseM = Triangle.inverseModel(res, a, b);
            rModelL.Lines = inverseM[0];
            rModelR.Lines = inverseM[1];

            bool aLExists = false;
            if (mask[0] != mask[1])
            {
                infoText.AppendFormat("αL = {0}\r\n", res[1][1]);
                aLExists = true;
            }
            if (mask[1] != mask[2])
            {
                infoText.AppendFormat("αR = {0}\r\n", res[2 + (aLExists ? 1 : 0)][1]);
            }

            info.Text = infoText.ToString();

            paintChart();

           

        }
    }
}
